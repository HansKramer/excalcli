#! /usr/bin/python
#
# \Author  Hans Kramer
#
# \Date    Nov  2018
#

from exchangelib         import Credentials, Account, EWSDateTime, EWSTimeZone, Configuration
from exchangelib.folders import Calendar

import errno


def get_gnomekeyring_password(keyname):
    keyring = "login"
    for id in gnomekeyring.list_item_ids_sync(keyring): 
        item = gnomekeyring.item_get_info_sync(keyring, id)
        if item.get_display_name() == keyname:
            return item.get_secret()
    return None


def use_evolution_settings(args):
    req_sections    = set(("Data Source", "Ews Backend", "Authentication"))
    req_data_source = set(("enabled", "displayname", "parent"))
    source_path     = os.path.expanduser("~/.config/evolution/sources")
    try:
        for source in os.listdir(source_path):
            cp = configparser.ConfigParser()
            source_file = os.path.join(source_path, source)
            cp.read(source_file)
            if set(cp.sections()) >= req_sections and set(cp["Data Source"]) >= req_data_source and \
               cp["Data Source"].getboolean("Enabled") and cp["Data Source"]["parent"]=="" and cp["Data Source"]["displayname"] == args.displayname:
                    args.server   = cp["Ews Backend"]["Hosturl"]
                    args.user     = cp["Authentication"]["User"]
                    args.email    = cp["Ews Backend"]["email"]
                    keyname       = 'Evolution Data Source "%s" (Collection - ews) ' % cp["Ews Backend"]["email"]
                    args.password = get_gnomekeyring_password(keyname)
                    return True                
    except OSError as e:
        if e.errno in (errno.ENOENT, errno.EACCES):
            return False
        # unexpected exception (yeah right... exceptions)
        raise e

    return False


def get_account(args):
    credentials = Credentials(args.user, args.password)
    config      = Configuration(service_endpoint=args.server, credentials=credentials)
    return Account(args.email, config=config, credentials=credentials)


def get_meetings(account, start, stop):
    meetings = {} 
    for x in account.calendar.view(start=start, end=end):
        dt = x.start.astimezone(tz)
        if not meetings.has_key(dt.day):
            meetings[dt.day] = {}
        meetings[dt.day]["%02d:%02d" % (dt.hour, dt.minute)] = x.subject
    return meetings


# horror code
def print_meetings(meetings):
    import calendar

    cal = calendar.Calendar(0)
    week      = []
    max_lines = 0
    
    column_width = 16
    format_width = 7*column_width + 6

    print u"${color #ff0000}\u250f" + u"\u2501" * format_width + u"\u2513"
    date_title = "%s %d" % (calendar.month_name[today.month], today.year)
    padding    = " " * (format_width - len(date_title))
    print u"${color #ff0000}\u2503${color yellow}%s%s${color #ff0000}\u2503" % (date_title, padding)

    line_segment = u"\u2500"*column_width + u"\u252c" 
    print u"${color #ff0000}\u2520" + line_segment*6 + u"\u2500"*column_width + u"\u252b"

    skip = 1
    for d in cal.itermonthdates(today.year, today.month):
        if not skip and len(week) == 0:
            line_segment = u"\u2500"*column_width + u"\u253c" 
            print u"${color #ff0000}\u2520" + line_segment*6 + u"\u2500"*column_width + u"\u252b"
        if d.month == today.month: 
            day = [("${color yellow}", "%02d" % d.day)]
            if meetings.has_key(d.day):
                for item in sorted(meetings[d.day].keys()):
                    day += [("${color #ffffff}", item)]
                    day += textwrap.wrap(meetings[d.day][item], column_width) 
                if len(day) > max_lines:
                    max_lines = len(day)
            week += [day]
        else:
            week += [[]]
        if len(week) == 7:
            for i in range(max_lines):
                line = u"\u2503"
                cnt  = 0
                for day in week:
                    if i < len(day):
                        if type(day[i]) == types.TupleType:
                            line += "%s%-16s${color #ff0000}" % day[i]
                        else:
                            line += "${color #aa0000}%-16s${color #ff0000}" % day[i]
                    else:
                        line += "%-16s" % " "
                    cnt += 1
                    if cnt == 7:
                        line += u"\u2503"
                    else:
                        line += u"\u2502"
                print line
            week      = []
            max_lines = 0
            skip      = 0
    line_segment = u"\u2501"*column_width + u"\u2537" 
    print u"${color #ff0000}\u2517" + line_segment*6 + u"\u2501"*column_width + u"\u251b"


def get_password_from_gnomekeyring(args):
    keyring = "login"
    keyname = args.webmail
    for id in gnomekeyring.list_item_ids_sync(keyring): 
        item = gnomekeyring.item_get_info_sync(keyring, id)
        if item.get_display_name() == keyname:
            args.password = item.get_secret()
            return True
    return False


def store_password(args):
    keyring.set_password("excalcli", args.user, args.password)


def get_password_from_keyring(args):
    args.password = keyring.get_password("excalcli", args.user)


# XXX error handling dude!!!
def conky_print_basic_calendar(args, start, end):
    account = get_account(args)
    meetings = get_meetings(account, start, end)
    print_meetings(meetings)
    if args.store_password:
        store_password(args)


if __name__ == "__main__":
    import argparse
    import sys
    import datetime
    import dateutil
    import textwrap
    import types
    import os

    import keyring
    import gnomekeyring 
    import configparser

    today      = datetime.date.today()
    next_month = today + dateutil.relativedelta.relativedelta(months=1)

    tz    = EWSTimeZone.localzone()
    start = tz.localize(EWSDateTime(today.year, today.month, 1,  0, 0, 0))
    end   = tz.localize(EWSDateTime(next_month.year, next_month.month, 1, 0, 0, 0))

    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user",           required=False)
    parser.add_argument("-p", "--password",       required=False)
    parser.add_argument("-s", "--server",         required=False)
    parser.add_argument("-e", "--email",          required=False)
    parser.add_argument("-E", "--evolution",      required=False, action="store_true")
    parser.add_argument("-d", "--displayname",    required=False)
    parser.add_argument("-w", "--webmail",        required=False)
    parser.add_argument("-S", "--store-password", required=False, action="store_true")
    args = parser.parse_args()

    if args.evolution:
        use_evolution_settings(args)
    else:
        if None in (args.user, args.server, args.email):
            parser.print_help()
            sys.exit(-1)
        if not args.password:
            if args.webmail: # get password from gnome keyring
                get_password_from_gnomekeyring(args)
            else:
                get_password_from_keyring(args)
    conky_print_basic_calendar(args, start, end)

